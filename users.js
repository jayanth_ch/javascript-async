function fetchData(data) {
    return new Promise(resolve => setTimeout(() => {
        resolve(data)
    }, 1000))
}

function sendUserLoginRequest(userId) {
    console.log(`Sending login request for ${userId}...`);
    return fetchData(userId).then(() => {
        console.log('Login successfull.', userId);
        return userId;
    })
}

function getUserProfile(userId) {
    console.log(`Fetching profile for ${userId}...`);
    return fetchData({
        user1: { name: 'Vijay', points: 100 },
        user2: { name: 'Sahana', points: 200 }
    }).then(profiles => {
        console.log(`Received profile for ${userId}`);
        return profiles[userId];
    });
}

function getUserPosts(userId) {
    console.log(`Fetching posts for ${userId}...`);
    return fetchData({
        user1: [{ id: 1, title: 'Economics 101' }, { id: 2, title: 'How to negotiate' }],
        user2: [{ id: 3, title: 'CSS Animations' }, { id: 4, title: 'Understanding event loop' }]
    }).then(posts => {
        console.log(`Received posts for ${userId}`);
        return posts[userId];
    });
}


//............................task 1..............................

async function userDataSerial() {
    try {
        console.time('userData-serial')
        // Write code here
        const userId = await sendUserLoginRequest('user1')
        //console.log(user1)
        const userProfile = await getUserProfile(userId)
        console.log(userProfile, "profile")
        const userPosts = await getUserPosts(userId)
        console.log(userPosts, "post")
        console.timeEnd('userData-serial')
    } catch (err) {
        console.log(err)
    }
}

//.....................task 2........................................
async function userDataParallel() {
    console.time('userData-parallel');
    // Write code here
    try {
        const userId = await sendUserLoginRequest('user2');
        const userProfile = await getUserProfile(userId);
        const userPosts = await getUserPosts(userId);

        console.log(userProfile);
        console.log(userPosts);
    } catch (error) {
        console.log(error);
    }
    console.timeEnd('userData-parallel');
}

userDataSerial()
userDataParallel()
