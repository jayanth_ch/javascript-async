function fetchRandomNumbers(callback) {
    return new Promise((resolve, reject) => {

        console.log('Fetching number...');
        setTimeout(() => {
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            console.log('Received random number:', randomNum);
            resolve(randomNum);
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}

function fetchRandomString(callback) {
    return new Promise((resolve, reject) => {

        console.log('Fetching string...');
        setTimeout(() => {
            let result = '';
            let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for (let i = 0; i < 5; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }

            console.log('Received random string:', result);
            resolve(result);

        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}



// fetchRandomNumbers((randomNum) => console.log(randomNum))
// fetchRandomString((randomStr) => console.log(randomStr))
//......................task 1.....................................................
async function fetchNumAndString() {
    try {
        const randomNumber = await fetchRandomNumbers()
        console.log(randomNumber)

        const randomString = await fetchRandomString()
        console.log(randomString)
    } catch (err) {
        console.log(err)
    }
}

fetchNumAndString()
//...............................task 2....................................
async function fetchAndAdd() {
    try {
        let sum = 0;
        const randomNumber = await fetchRandomNumbers();
        sum += randomNumber
        console.log("ranNum", sum)
        const randomString = await fetchRandomString();
        sum += randomString
        console.log("ranStr", sum)

    } catch (error) {
        console.log(error)
    }
}
fetchAndAdd()
//................................task 3....................................
async function concatNumAndString() {
    try {
        let add = '';
        const randomNumber = await fetchRandomNumbers()
        add += randomNumber;
        const randomString = await fetchRandomString();
        add += randomString;
        console.log("concated string", add);
    } catch (error) {
        console.log(error);
    }
}
concatNumAndString();
//..................................task 4...............................
async function random10NumberSum() {
    try {
        let sum = 0;
        for (let i = 0; i < 10; i++) {
            const randomNumber = await fetchRandomNumbers();
            sum += randomNumber
        }
        console.log("10", sum)
    }
    catch (error) {
        console.log(error)
    }
}
random10NumberSum()